import { Server, Socket } from "socket.io";
import { texts } from "../data";

import * as config from "./config";

const connections: Socket[] = [];
const startedAndFullRooms = new Set<string>();

interface IUser {
  username: string;
  progress: number;
  ready: boolean;
  finishTime: number;
}

export const usersOnline: {
  [socketId: string]: IUser;
} = {};

function randomInteger(min = 0, max) {
  return Math.floor(min + Math.random() * (max - min));
}

function roomsToArray(rooms: Map<string, Set<string>>) {
  const newRooms: { name: string; users: IUser[] }[] = [];

  for (let [key, value] of rooms) {
    if (key.length < 20 && !startedAndFullRooms.has(key)) {
      const usersSocketId = Array.from(value);
      const users: IUser[] = [];

      for (let val of usersSocketId) {
        if (usersOnline[val]) {
          users.push(usersOnline[val]);
        }
      }

      newRooms.push({ name: key, users });
    }
  }

  return newRooms;
}

export const socketHandler = (io: Server) => {
  io.on("connection", (socket) => {
    const { username } = socket.handshake.query;

    usersOnline[socket.id] = {
      username: username as string,
      progress: 0,
      ready: false,
      finishTime: 0,
    };

    const rooms = io.sockets.adapter.rooms;

    socket.emit("serverRooms", roomsToArray(rooms));

    socket.on("joinToRoom", (data: { name: string }) => {
      const { name } = data;

      const room = io.sockets.adapter.rooms.get(name);

      if (
        room &&
        Array.from(room).length - 1 === config.MAXIMUM_USERS_FOR_ONE_ROOM
      ) {
        startedAndFullRooms.add(name);
        socket.emit("serverSendMessage", {
          message: "Maximum users is room!!!",
        });
        return;
      }

      if (room) {
        room.add(socket.id);

        const roomUsers = Array.from(room);
        const users: IUser[] = [];

        for (let [key, val] of Object.entries(usersOnline)) {
          if (roomUsers.includes(key)) {
            users.push(val);
          }
        }

        io.in(name).emit("joinToRoomResult", { name, users });
      }
    });

    socket.on("getUsersFromRoom", (data: { roomName: string }) => {
      const { roomName } = data;

      const usersInRoom = io.sockets.adapter.rooms.get(roomName);
      const users: IUser[] = [];

      if (usersInRoom) {
        for (let value of usersInRoom) {
          users.push(usersOnline[value]);
        }
        if (users.length) {
          io.in(roomName).emit("getUsersFromRoomResult", { users });
        }
      }
    });

    socket.on(
      "setUserProgress",
      (data: {
        username: string;
        progress: number;
        roomName: string;
        time: number;
      }) => {
        const { username, progress, roomName, time } = data;

        const usersInRoom = io.sockets.adapter.rooms.get(roomName);
        let user: IUser | null = null;
        const usersProgress: number[] = [];

        if (usersInRoom) {
          for (let value of usersInRoom) {
            if ((usersOnline[value].username = username)) {
              usersOnline[value].progress = progress;
              if (progress === 100) {
                usersOnline[value].finishTime = time;
              }
              user = usersOnline[value];
            }
            usersProgress.push(usersOnline[value].progress);
          }
          if (user) {
            const isFinish = usersProgress.every((val) => val === 100);
            io.in(roomName).emit("setUserProgressResult", { user, isFinish });
          }
        }
      }
    );

    socket.on("resetUsersData", (data: { roomName: string }) => {
      const { roomName } = data;

      const usersInRoom = io.sockets.adapter.rooms.get(roomName);
      const resetedUsers: IUser[] = [];

      if (usersInRoom) {
        for (let value of usersInRoom) {
          usersOnline[value].ready = false;
          usersOnline[value].progress = 0;
          usersOnline[value].finishTime = 0;

          resetedUsers.push(usersOnline[value]);
        }

        io.in(roomName).emit("resetUsersDataResult", {
          users: resetedUsers,
          roomName,
        });
      }
    });

    socket.on("addNewRoom", (data: { name: string }) => {
      const { name } = data;

      const room = io.sockets.adapter.rooms.get(name);
      if (room) {
        socket.emit("serverSendMessage", {
          message: "This name is alredy exist!!!",
        });
        return;
      }

      if (name.length < 20) {
        const newRoom = new Set<string>().add(socket.id);
        io.sockets.adapter.rooms.set(name, newRoom);

        const users: IUser[] = [];

        for (let [key, val] of Object.entries(usersOnline)) {
          if (key === socket.id) {
            users.push(val);
          }
        }

        io.emit("serverRooms", [{ name, users }]);
      } else {
        socket.emit("serverSendMessage", {
          message: "Name must be less 20 symbols!!!",
        });
      }
    });

    socket.on("setIsReadyUser", (data) => {
      const { username, roomName } = data;

      let user: IUser | null = null;

      const isReadyUsers: boolean[] = [];
      const usersInRoom = io.sockets.adapter.rooms.get(roomName);

      if (usersInRoom) {
        for (let value of usersInRoom) {
          if ((usersOnline[value].username = username)) {
            usersOnline[value].ready = true;
            user = usersOnline[value];
          }
          isReadyUsers.push(usersOnline[value].ready);
        }

        const start = isReadyUsers.every((val) => val);

        const response: {
          user: IUser | null;
          textId: number | boolean;
          seconds?: number;
          secondsForGame?: number;
        } = {
          user,
          textId: false,
        };

        if (start) {
          response.textId = randomInteger(0, texts.length - 1);
          response.seconds = config.SECONDS_TIMER_BEFORE_START_GAME;
          response.secondsForGame = config.SECONDS_FOR_GAME;
        }

        if (user) {
          io.in(roomName).emit("isReadyUserResult", response);
        }
      }
    });

    socket.on("disconnect", () => {
      const rooms = io.sockets.adapter.rooms;
      let isFinishConpetition = false;

      for (let [key, value] of rooms) {
        const users: IUser[] = [];

        for (let [k, val] of Object.entries(usersOnline)) {
          if (value.has(k)) {
            users.push(val);
            if (val.finishTime === 100) {
              isFinishConpetition = true;
            }
          }
        }

        const response: {
          users: IUser[];
          textId: number | boolean;
          seconds?: number;
          secondsForGame?: number;
          isFinishConpetition?: boolean;
        } = {
          users,
          textId: false,
        };

        if (isFinishConpetition) {
          response.isFinishConpetition = isFinishConpetition;
        } else {
          const start = users.every((val) => val.ready);

          if (start) {
            response.textId = randomInteger(0, texts.length - 1);
            response.seconds = config.SECONDS_TIMER_BEFORE_START_GAME;
            response.secondsForGame = config.SECONDS_FOR_GAME;
          }
        }
        io.to(key).emit("disconnectParticipant", response);
        socket.leave(key);
      }

      io.emit("serverRooms", roomsToArray(rooms));

      connections.splice(connections.indexOf(socket), 1);
      delete usersOnline[socket.id];
    });
  });
};
