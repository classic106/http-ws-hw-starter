import { Router } from "express";
import { usersOnline } from "../socket";

const router = Router();

router.get("/", (req, res) => {
  const { name } = req.query;

  if (usersOnline.hasOwnProperty(name as string)) {
    res.json(true);
    return;
  }
  res.json(false);
});

export default router;
