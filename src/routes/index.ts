import { Express } from "express";
import loginRoutes from "./loginRoutes";
import gameRoutes from "./gameRoutes";
import checkName from "./checkName";
import getTextById from "./getTextById";

export default (app: Express) => {
  app.use("/login", loginRoutes);
  app.use("/game", gameRoutes);
  app.use("/checkname", checkName);
  app.use("/game/texts", getTextById);
};
