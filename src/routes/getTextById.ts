import { Router } from "express";
import { texts } from "../data";

const router = Router();

router.get("/", (req, res) => {
  const { id } = req.query;

  if (id) {
    res.json(texts[+id]);
    return;
  }
  res.json(false);
});

export default router;
