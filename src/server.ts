import express from "express";
import http from "http";
import { Server } from "socket.io";
import { STATIC_PATH, PORT } from "./config";
import { socketHandler } from "./socket";
import routes from "./routes";

const app = express();
const httpServer = new http.Server(app);
const socketIo = new Server(httpServer);

app.use(express.static(STATIC_PATH));

routes(app);

app.get("*", (req, res) => res.redirect("/login"));

httpServer.listen(PORT, () => console.log(`Listen server on port ${PORT}`));
socketHandler(socketIo);

export default { app, httpServer };
