import { renderUsers } from "./renderUsers.mjs";
import { showMessageModal } from "./views/modal.mjs";
import { socket } from "./socket.mjs";
import { keyDown, keyUp } from "./helpers/keyboardEvents.mjs";

class RoomBehaviour {
  timer = document.querySelector("#timer");
  gamePage = document.querySelector("#game-page");
  readyBtn = document.querySelector("#ready-btn");
  gameTimer = document.querySelector("#game-timer");
  roomsPage = document.querySelector("#rooms-page");
  quitRoomBtn = document.querySelector("#quit-room-btn");
  textContainer = document.querySelector("#text-container");
  textContainer = document.querySelector("#text-container");
  gameTimerSeconds = document.querySelector("#game-timer-seconds");

  roomName = null;
  timerCompetition = null;
  timerBeforeCompetiion = null;

  resetRoom() {
    this.gameTimer.style.display = "none";
    this.quitRoomBtn.classList.add("display-none");
    this.readyBtn.classList.remove("display-none");
    this.textContainer.classList.add("display-none");
    this.quitRoomBtn.classList.toggle("display-none");
    socket.resetUsersDataInRoom(this.roomName);
  }

  finishCompetiton() {
    document.removeEventListener("keydown", keyDown);
    document.removeEventListener("keyup", keyUp);

    clearInterval(this.timerCompetition);
    this.timerCompetition = null;
    socket.getUsersFromRoom(this.roomName);
  }

  openRoom(data) {
    const { name, users } = data;

    this.roomName = name;

    this.roomsPage.classList.toggle("hidden");
    this.gamePage.classList.toggle("hidden");

    const roomName = document.querySelector("#room-name");
    roomName.innerText = name;

    const usersWrapper = document.querySelector("#users-wrapper");
    usersWrapper.innerHTML = "";

    renderUsers(name, users);
  }

  closeRoom() {
    this.roomsPage.classList.toggle("hidden");
    this.gamePage.classList.toggle("hidden");
  }

  startCompetition(text, secondsForGame) {
    this.gameTimer.style.display = "flex";
    this.gameTimerSeconds.innerText = secondsForGame;

    document.addEventListener(
      "keydown",
      keyDown.bind({ ...this, text, secondsForGame })
    );

    document.addEventListener("keyup", keyUp);

    this.timerCompetition = setInterval(() => {
      this.gameTimerSeconds.innerText = secondsForGame;

      if (secondsForGame < 1) {
        this.finishCompetiton();
        return;
      }
      secondsForGame--;
    }, 1000);
  }

  startBeforeCompetition(text, seconds, secondsForGame) {
    if (text && seconds && secondsForGame) {
      this.timer.innerText = seconds;

      this.readyBtn.classList.toggle("display-none");
      this.quitRoomBtn.classList.toggle("display-none");
      this.timer.classList.toggle("display-none");

      this.timerBeforeCompetiion = setInterval(() => {
        this.timer.innerText = seconds;

        if (seconds < 0) {
          clearInterval(this.timerBeforeCompetiion);
          this.timerBeforeCompetiion = null;
          this.textContainer.classList.toggle("display-none");
          this.textContainer.innerText = text;
          this.timer.classList.toggle("display-none");
          this.startCompetition(text, secondsForGame);
          return;
        }
        seconds--;
      }, 1000);
    } else {
      showMessageModal({ messge: "Text is undefined!!!" });
    }
  }
}

const roomBehaviour = new RoomBehaviour();

export { roomBehaviour };
