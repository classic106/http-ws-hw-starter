import { socket } from "./socket.mjs";
import { appendRoomElement } from "./views/room.mjs";

function renderRomms(rooms) {
  const roomsWrapper = document.querySelector("#rooms-wrapper");
  roomsWrapper.innerHTML = "";

  for (let val of rooms) {
    const { name, users } = val;

    function onJoin() {
      socket.joinToRoom(name);
    }
    appendRoomElement({ name, numberOfUsers: users.length, onJoin });
  }
}

export { renderRomms };
