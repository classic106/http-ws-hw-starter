import { socket } from "./socket.mjs";
import { showInputModal, showMessageModal } from "./views/modal.mjs";
import { roomBehaviour } from "./roomBehaviour.mjs";

const username = sessionStorage.getItem("username");

if (!username) {
  window.location.replace("/login");
}

if (username) {
  socket.connection(username);
  const addRoomButton = document.querySelector("#add-room-btn");

  const quitRoomBtn = document.querySelector("#quit-room-btn");
  quitRoomBtn.addEventListener("click", () => roomBehaviour.closeRoom());

  addRoomButton.addEventListener("click", () => {
    function onSubmit(value) {
      if (value) {
        socket.addNewRoom(value);
      } else {
        showMessageModal({ message: "Empty field!!!" });
      }
    }

    showInputModal({ title: "Create New", onSubmit });
  });
}
