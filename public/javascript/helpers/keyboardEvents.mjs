import { setProgress } from "../views/user.mjs";
import { socket } from "../socket.mjs";
import { mapKeyPressToActualCharacter } from "./mapKeyPressToActualCharacter.mjs";

const username = sessionStorage.getItem("username");

let index = 0;
let isPressShift = false;

function keyDown(e) {
  const { text } = this;
  const textAsArr = text.split("");

  if (e.which === 16) {
    isPressShift = true;
  }

  const inputChar = mapKeyPressToActualCharacter(isPressShift, e.which);

  if (textAsArr[index] === inputChar) {
    index++;
    const progress = (index * 100) / textAsArr.length;
    const progressText = `<span class="progress">${textAsArr
      .slice(0, Math.max(index))
      .join("")}</span>`;
    const underlineText = `<span class="underline">${textAsArr[index]}</span>`;
    const restText = `${textAsArr
      .slice(Math.max(index + 1), textAsArr.length)
      .join("")}`;

    let newText = progressText;

    if (textAsArr[index]) {
      newText += underlineText;
    }

    if (restText) {
      newText += restText;
    }

    this.textContainer.innerHTML = newText;
    setProgress({ username, progress });
    socket.setUserProgress(
      username,
      progress,
      this.roomName,
      this.secondsForGame
    );
  }
}

function keyUp(e) {
  if (e.which === 16) {
    isPressShift = false;
  }
}

export { keyDown, keyUp };
