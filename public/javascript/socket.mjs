import { showMessageModal, showResultsModal } from "./views/modal.mjs";
import { renderRomms } from "./renderRooms.mjs";
import { roomBehaviour } from "./roomBehaviour.mjs";
import { changeReadyStatus } from "./views/user.mjs";
import { renderUsers } from "./renderUsers.mjs";
import { setProgress } from "./views/user.mjs";

function getText(id) {
  return fetch(`http://localhost:3002/game/texts?id=${id}`).then((res) =>
    res.json()
  );
}

class Socket {
  socket = null;

  async connection(username) {
    this.socket = await io("http://localhost:3002", {
      reconnectionDelayMax: 10000,
      query: { username },
    });

    this.mountSocketEvents();
  }

  addNewRoom(name) {
    this.socket.emit("addNewRoom", { name });
  }

  joinToRoom(name) {
    this.socket.emit("joinToRoom", { name });
  }

  setIsReadyUser(roomName, username) {
    this.socket.emit("setIsReadyUser", { username, roomName });
  }

  resetUsersDataInRoom(roomName) {
    this.socket.emit("resetUsersData", { roomName });
  }

  getUsersFromRoom(roomName) {
    this.socket.emit("getUsersFromRoom", { roomName });
  }

  setUserProgress(username, progress, roomName, time) {
    this.socket.emit("setUserProgress", { username, progress, roomName, time });
  }

  dicconnect() {
    if (this.socket) {
      this.socket.dicconnect();
      this.socket = null;
      sessionStorage.removeItem("username");
    }
  }

  mountSocketEvents() {
    this.socket.on("disconnectParticipant", async (data) => {
      const { users, textId, seconds, secondsForGame, isFinishConpetition } =
        data;

      renderUsers(users);

      if (isFinishConpetition) {
        roomBehaviour.finishCompetiton();
      } else if (textId) {
        const text = await getText(textId);
        roomBehaviour.startBeforeCompetition(text, seconds, secondsForGame);
      }
    });

    this.socket.on("isReadyUserResult", async (data) => {
      if (data) {
        const { user, textId, seconds, secondsForGame } = data;
        const { username, ready } = user;

        changeReadyStatus({ username, ready });

        if (textId) {
          const text = await getText(textId);
          roomBehaviour.startBeforeCompetition(text, seconds, secondsForGame);
        }
      }
    });

    this.socket.on("resetUsersDataResult", (data) => {
      const { users, roomName } = data;
      renderUsers(roomName, users);
    });

    this.socket.on("joinToRoomResult", (data) => {
      if (data) {
        roomBehaviour.openRoom(data);
      }
    });

    this.socket.on("serverSendMessage", (data) => {
      const { message } = data;
      showMessageModal({ message });
    });

    this.socket.on("serverRooms", (data) => {
      renderRomms(data);
    });

    this.socket.on("setUserProgressResult", (data) => {
      const { user, isFinish } = data;
      const { username, progress } = user;

      setProgress({ username, progress });

      if (isFinish) {
        roomBehaviour.finishCompetiton();
      }
    });

    this.socket.on("getUsersFromRoomResult", (data) => {
      const { users } = data;

      users.sort((first, second) => {
        if (first.finishTime < second.finishTime) return -1;
        if (first.finishTime > second.finishTime) return 1;
        return 0;
      });

      const usersNames = users.map((user) => user.username);
      showResultsModal({
        usersSortedArray: usersNames,
        onClose: roomBehaviour.resetRoom.bind(roomBehaviour),
      });
    });
  }
}

const socket = new Socket();

export { socket };
