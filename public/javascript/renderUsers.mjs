import { socket } from "./socket.mjs";
import { appendUserElement } from "./views/user.mjs";

function onClick() {
  socket.setIsReadyUser(this.rooomName, this.username);
  this.readyBtn.innerText = "READY";
}

function renderUsers(rooomName, users) {
  const readyBtn = document.querySelector("#ready-btn");
  const usersWrapper = document.querySelector("#users-wrapper");

  usersWrapper.innerHTML = "";

  users.forEach((user) => {
    const { username, ready } = user;

    const currentUser = sessionStorage.getItem("username");

    let isCurrentUser = false;

    if (currentUser && username === currentUser) {
      isCurrentUser = true;

      if (!ready) {
        readyBtn.innerText = "READY";
        readyBtn.onclick = onClick.bind({ rooomName, username, readyBtn });
      } else {
        readyBtn.innerText = "NOT READY";
      }
    }

    appendUserElement({ username, ready, isCurrentUser });
  });
}

export { renderUsers };
